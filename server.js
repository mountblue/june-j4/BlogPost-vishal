const express = require("express");
const app = express();
const path = require("path");
const mongoose = require("mongoose");
const posts = require("./backend/schema");
const port = 8080;
const bodyParser = require("body-parser");

mongoose.connect("mongodb://localhost:27017/testredux", { useNewUrlParser: true });
mongoose.Promise = Promise;
var db = mongoose.connection;

app.use(express.static(__dirname + "/build"));
app.use(express.static("build/static"));
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.get('/api/posts', (req, resp) => {
  db.collection('posts').find({}).toArray((error, posts) => {
    resp.json({ posts });
  });
});

app.use(bodyParser.json());


app.use(function(req, res, next) { res.header("Access-Control-Allow-Origin", "*"); res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); next(); });
app.get('/',(req,res)=>{
  res.sendfile(__dirname,'build/index.html');
})

app.post('/api/posts', (req, resp) => {
  console.log(req.body)
  posts.create(req.body).then(function (data) {
    resp.json(data);
    console.log(data);
  });
});

app.delete("/api/posts/:id", function (req, res) {
  posts.findByIdAndRemove({ _id: req.params.id }).then(function (blog) {
    res.send({ blog: "deleted" });
  });
});

app.put("/api/posts/:id", function (req, res) {
  posts.findByIdAndUpdate(req.body._id, { text: req.body.text, description: req.body.description },
    function (err) {
      if (err) {
        res.send(err);
        return;
      }
      res.send({ data: "blog has been Updated..!!" });
    });
});


app.use((req, res) => {
  res.status(404).json({
    errors: {
      global: "Still working on it. Please try again later when we implement it"
    }
  });
});

app.get('*', function (req, res) {
  res.sendfile(__dirname + "/build/index.html");
});

app.listen(port, () => console.log(`server is running on localhost ${port}`));
