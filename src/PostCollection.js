import React from "react";
import {Link} from 'react-router-dom'
import { connect } from 'react-redux'

class PostLists extends React.Component {

    render() {
        const blogList = this.props.blog.filter(item => item._id === this.props.match.params.id);
        console.log( this.props.blog,"new bloglist")
        return (
            <div>
                <h1>{blogList[0].text}</h1>
                <br />
                <br />
                {console.log(blogList[0]._id)}
                <h3> {blogList[0].description} </h3>
                <br />
                <Link to="/"> <button> back </button> </Link>
            </div>

        );
    }
};

function mapStateToProps(state) {
    return {
        blog: state.blog
    }
}


export default connect(mapStateToProps)(PostLists)
