import React from "react";
import BlogList from './BlogList'
import {connect} from 'react-redux'
import {fetchBlog} from './actions'


class HomePage extends React.Component {

    componentDidMount(){
        this.props.fetchBlog();
    }

    render() {{console.log(this.props.blog, "blogs")}
        return (
            <div>
                <h1>
                    blog list
                </h1>
                <BlogList />
            </div>
        )
    }
}


function mapStateToProps(state){
    return {
        blog:state.blog,
    }
}

export default connect(mapStateToProps,{fetchBlog})(HomePage)