import React from "react";
import { connect } from 'react-redux'
import { editblog } from './actions';
import { Link, Redirect } from 'react-router-dom'

class Post extends React.Component {
    constructor() {
        super();

        this.state = {
            blogList: [],
            text: "",
            description: "",
            editItem:[]
        };
        this.handleTitle = this.handleTitle.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
       var  editItem = this.props.blog.filter(item => item._id === this.props.match.params.id);
        this.setState({
            text : editItem[0].text,
            description:editItem[0].description
        })
    }

    async onSubmit(event) {
        event.preventDefault();
            const obj={
                _id:this.props.match.params.id,
                text:this.state.text,
                description:this.state.description
            }

        this.setState({
            text: "",
            description: ""
        });
       await this.props.editblog(obj);
        this.props.history.push('/')
    }

    handleTitle(event) {
        this.setState({
            text: event.target.value,
        });

    }
    handleDescription(event) {
        this.setState({
            description: event.target.value
        });
    }

    render() {
    
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <textarea
                        id="blog-title"
                        value={this.state.text}
                        onChange={this.handleTitle}
                        placeholder="text here"
                    />
                  
                
                    <textarea
                        id="blog-content"
                        value={this.state.description}
                        onChange={this.handleDescription}
                        placeholder="desp here"
                     />
                    <button >save</button>
                    <Link to="/"> <button> back </button> </Link>
                </form>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        blog: state.blog
    }
}


export default connect(mapStateToProps, { editblog })(Post)