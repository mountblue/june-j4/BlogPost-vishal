export default function blog(state = [], action = {}) {
    switch (action.type) {
        case "ADD_DESCRIPTION":
            { console.log("Entered reducer") }
            let temp = {
                text: action.text,
                description: action.description,
                _id: action._id,
            }
            console.log(action)
            return [temp, ...state]

        case "DELETE_DESCRIPTION":
            { console.log("Entered delete reducer") }
            return state.filter(item => item._id !== action._toDeleteBlogId);

        case "GET_ALL_POST":
            action.payload.sort(function (a, b) {
                return a._id < b._id;
            });
            { console.log(action.payload, "GET_ALL_POST") }
            return action.payload;

        case "UPDATE_DESCRIPTION":
            { console.log("Entered update reducer") }
            return (
                state.filter(item => item._id === action._id ? (item.text = action.text, item.description = action.description) : true)
            );

        default: return state;
    }
}

