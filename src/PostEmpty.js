import React from "react";
import {Link} from 'react-router-dom'

class PostEmpty extends React.Component {

    render() {
        return (
            <div>
                <h2>"No blog to display"</h2>
                <Link to="/"> <button> back </button> </Link>
            </div>

        );
    }
};


export default PostEmpty
