export function fetchBlog() {
    return dispatch => {
        fetch('http://localhost:8080/api/posts',{
            headers: {
                "Access-Control-Allow-Origin" : "*",
                "Content-type": 'application/json'
            }
        })
            .then(res => res.json())
            .then(data => {
                dispatch(getAllBlogs(data))
                console.log(data, "fetchPOst")
            });
    }
}

export function savePosts(data) {
    console.log(data , "insied savePost");
    return dispatch => {
        fetch('http://localhost:8080/api/posts', {
            method: 'POST',
            body: JSON.stringify(data) ,
            headers: {
                "Access-Control-Allow-Origin" : "*",
                "Content-type": 'application/json'
            }
        }).then(res => res.json())
            .then(data => {
                console.log(data +  "Checking saved data")
                dispatch(createPost(data))
                console.log(data)
            });
    }
}

export function deleteBlog(id) {
    return dispatch => {
        fetch(`http://localhost:8080/api/posts/${id}`, {
            method: 'DELETE',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                "Content-type": 'application/json'
            }
        }).then(res => res.json())
            .then(data => {
                dispatch(blogDeleteDisp(id))
            });
    }
}

export function editblog(blog) {
    let updateblog = {
        _id: blog._id,
        text: blog.text,
        description: blog.description
    }
    console.log(blog)
    return dispatch => {
        fetch(`http://localhost:8080/api/posts/${blog.id}`, {
            method: "PUT",
            headers: {
                "Access-Control-Allow-Origin" : "*",
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(updateblog),
        })
            .then(res => res.json())
            .then(data => {
                dispatch(editBlogDisp(blog._id, blog.text, blog.description))
                console.log(data, "updatedblog")
            });
    }
}

export function getAllBlogs(data) {
    data = data.posts;
    let newBlog = data.map(item => {
        delete item["__v"];
        return item;
    })
    return {
        type: "GET_ALL_POST",
        payload: newBlog
    }
}

export function createPost(data) {
    return {
        type: "ADD_DESCRIPTION",
        text: data.text,
        description: data.description,
        _id: data._id,
    }
}

export function blogDeleteDisp(id) {
    return {
        type: "DELETE_DESCRIPTION",
        _toDeleteBlogId: id,
    }
}



export function editBlogDisp(id, text, description) {
    return {
        type: "UPDATE_DESCRIPTION",
        _id: id,
        text: text,
        description: description
    }
}