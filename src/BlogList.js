import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { deleteBlog } from './actions'

class BlogList extends React.Component {

    render() {
        const emptyMessage = (
            <p> there is  no blog as of now</p>
        )

        const onDelete = (event) => (
            this.props.deleteBlog(event.target.parentNode.id)
        );

        return (
            <div>
                {this.props.blog.length === 0 ? emptyMessage : this.props.blog.map((blog, index) => <div key={index} id={blog._id}><Link to={`/posts/${blog._id}`}> <button >{"edit"}</button> <p> {blog.text} </p> </Link>  <button onClick={onDelete}>{"delete"}</button>   <Link to={`/posts/${blog._id}/description`} ><button>{"view"}</button> </Link>  </div>)}
            </div>
        );
    }
}



function mapStateToProps(state) {
    return {
        blog: state.blog
    }
}


export default connect(mapStateToProps, { deleteBlog })(BlogList)