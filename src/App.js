import React, { Component } from 'react';
import './App.css';
import { BrowserRouter } from "react-router-dom";
import { Router, Route, Switch, Link } from "react-router-dom";
import AddBlogsPage from './AddBlogsPage';
import HomePage from './Home'
import Post from './Post'
import PostDesc from './PostCollection'
import PostEmpty from './PostEmpty'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div className="App">   
          <p className="App-intro">
            <Link to="/">Home</Link>
            <span />
            <Link to="/create"> AddBlog </Link>
            <span />
            <Link to="/posts"> Post </Link>
          </p>

          <Switch>
            <Route activeClassName="active" activeOnlyWhenExact path="/posts/:id/description" component={PostDesc} />
            <Route activeClassName="active" activeOnlyWhenExact path="/posts/:id" component={Post} />
            <Route activeClassName="active" activeOnlyWhenExact path="/posts" component={PostEmpty} />
            <Route activeClassName="active" activeOnlyWhenExact path="/create" component={AddBlogsPage} />
            <Route exact={true} activeClassName="active" activeOnlyWhenExact path="/" component={HomePage} />
            <Route path="*" render={()=>(<h1>NoMatch found 404 error</h1>)}/>
          </Switch>
        
      </div>
      </BrowserRouter>
    );
  }
}

export default App;
