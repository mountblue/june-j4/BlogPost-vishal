import React from "react";
import { connect } from 'react-redux'
import { savePosts } from './actions'
import { Redirect } from "react-router-dom"
var classnames = require("classnames");


class AddList extends React.Component {
    constructor() {
        super();
        this.handleChange1 = this.handleChange1.bind(this);
        this.handleChange2 = this.handleChange2.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
        this.state = {
            text: '',
            description: '',
            errors: {},
            done: false
        };
    }
    addPost(text, description, id) {
        return {
            type: 'ADD_DESCRIPTION',
            text,
            description,
            _id :id
        }
    }

    submitHandler(evt) {
        evt.preventDefault();

        //validation
        let errors = {};
        if (this.state.text === "") errors.text = "can't be empty";
        if (this.state.description === "") errors.description = "can't be empty"
        this.setState({ errors })
        const { text, description } = this.state;
         this.props.savePosts({text,description});
        
        this.setState({
            text: '',
            description: '',
            done : true
        });
    }

    handleChange1(event) {
        this.setState({
            text: event.target.value
        });
    }
    handleChange2(event) {
        this.setState({
            description: event.target.value
        });
    }

    render() {
        const form = (
            <div>
                <h1>
                    ADD blog here
            </h1>
                <form className="add-blog-container" onSubmit={this.submitHandler}>
                    <div className="input-tag1">
                        <input type="text"
                            id="theInput1"
                            value={this.state.text}
                            onChange={this.handleChange1}
                            placeholder={"add blog title here...."} />
                    </div>
                    <div className="input-tag2">
                        <input className="description" type="text"
                            id="theInput2"
                            value={this.state.description}
                            onChange={this.handleChange2}
                            placeholder={"add blog description here...."} />
                    </div>
                    <span > {this.state.errors.text} </span>
                    <input className="add-button" type="submit" value="ADD" />
                </form>
            </div>)
        return (
            <div>
            {this.state.done ? <Redirect to ="/" /> : form}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        blog: state.blog
    }
}


export default connect(mapStateToProps ,{savePosts})(AddList);