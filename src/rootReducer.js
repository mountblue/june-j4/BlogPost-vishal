import {combineReducers} from 'redux'
import blog from './reducers/Blog'

export default combineReducers({
    blog
})