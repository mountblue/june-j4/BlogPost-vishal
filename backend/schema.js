var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var BlogPostSchema = new Schema({
    id: String,
    text: String,
    description: String
});

module.exports = mongoose.model('posts',BlogPostSchema);
